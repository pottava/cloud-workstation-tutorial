# Streamlit サンプル

## Cloud Workstaions の起動

Cloud Shell で以下のコマンドを実行してください。

```sh
git clone https://gitlab.com/pottava/cloud-workstation-tutorial.git ~/cloud-workstation-tutorial && cd $_
teachme tutorial.md
```

## クライアントの認証とリソース ID の指定

`resource_id` は、ちゅーとるあるを実施するにあたって 1 つのプロジェクトの中であなたのリソースであることを示すものです。任意の文字で OK です。

```sh
gcloud auth login
export resource_id=<your-id>
```

## Google Cloud services

クラウド上利用するサービスを有効化します。

```sh
gcloud services enable compute.googleapis.com run.googleapis.com \
    artifactregistry.googleapis.com iamcredentials.googleapis.com
```

## Google Cloud Storage

CSV 管理用のバケットを作成します。

```sh
export project_id=$( gcloud config get-value project )
export bucket_name="csv-${project_id}-${resource_id}"
gcloud storage buckets create "gs://${bucket_name}" --location "asia-northeast1" \
    --uniform-bucket-level-access --public-access-prevention --enable-autoclass
```

### Cloud IAM

バケットにアクセスするためのアプリケーション用サービス アカウントを作成します。

```sh
gcloud iam service-accounts create "sa-app-${resource_id}" \
    --display-name "SA for the streamlit app" \
    --description "Service Account for the Streamlit application"
gcloud storage buckets add-iam-policy-binding "gs://${bucket_name}" \
    --member "serviceAccount:sa-app-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --role "roles/storage.admin"
```

### Cloud Run

公開 URL を取得するため、サンプルアプリケーションをデプロイしておきます。

```sh
gcloud run deploy "streamlit-${resource_id}" --region "asia-northeast1" \
    --platform "managed" --cpu 1.0 --memory 512Mi --image gcr.io/cloudrun/hello \
    --service-account "sa-app-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --execution-environment gen2 --allow-unauthenticated
gcloud run services add-iam-policy-binding "streamlit-${resource_id}" \
    --region "asia-northeast1" --member "allUsers" --role "roles/run.invoker"
gcloud run services describe "streamlit-${resource_id}" --region "asia-northeast1" \
    --format 'value(status.url)'
```

## ローカル開発

### アプリケーションの依存解決と起動

```sh
cd src
pip install poetry
poetry install --no-root
poetry run streamlit run app.py
```

Cloud Workstations で開発する場合は、Google のプロキシーが Websocket 未対応のため  
ローカルマシンから TCP トンネルを確立することで動作を確認できます。

```sh
cluster_name=
config_name=
workstation_name=
streamlit_port=8501

gcloud workstations start-tcp-tunnel --cluster "${cluster_name}" \
    --config "${config_name}" --region asia-northeast1 \
    "${workstation_name}" "${streamlit_port}" \
    --local-host-port ":${streamlit_port}"
```

## アプリケーションのデプロイ

Artifact Registry にリポジトリを作り

```sh
gcloud artifacts repositories create "apps-${resource_id}" \
    --repository-format docker --location asia-northeast1 \
    --description "${resource_id}'s apps"
```

アプリをビルド、リポジトリにプッシュします。

```sh
gcloud builds submit \
    --tag "asia-northeast1-docker.pkg.dev/${project_id}/apps-${resource_id}/streamlit:dev" \
    .
```

Cloud Run にデプロイし、アクセスするとエラーが表示されます。

```sh
gcloud run deploy "streamlit-${resource_id}" --region "asia-northeast1" \
    --image "asia-northeast1-docker.pkg.dev/${project_id}/apps-${resource_id}/streamlit:dev" \
    --service-account "sa-app-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --update-env-vars "BUCKET=${bucket_name},CSV_FILE_PATH=/work/data/001.csv"
```

Cloud Storage に CSV をアップロードし、アプリケーションをリロードしてみましょう。

```sh
cd ..
gcloud storage cp data/input.csv "gs://${bucket_name}/data/001.csv"
```

## CI / CD パイプライン

CI ツールで利用するサービスアカウントを作ります。

```sh
gcloud iam service-accounts create "sa-cicd-${resource_id}" \
    --display-name "SA for CI/CD" --description "Service Account for CI/CD pipelines"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:sa-cicd-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --role "roles/viewer"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:sa-cicd-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --role "roles/run.admin"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:sa-cicd-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --role "roles/storage.admin"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:sa-cicd-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --role "roles/artifactregistry.writer"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:sa-cicd-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --role "roles/cloudbuild.builds.editor"
gcloud iam service-accounts add-iam-policy-binding \
    sa-app-${resource_id}@${project_id}.iam.gserviceaccount.com \
    --member "serviceAccount:sa-cicd-${resource_id}@${project_id}.iam.gserviceaccount.com" \
    --role "roles/iam.serviceAccountUser"
```

CI ツールと連携する Workload Identity を作り

```sh
gcloud iam workload-identity-pools create "idpool-${resource_id}" --location "global" \
    --display-name "Identity pool for ${resource_id}"
idp_id=$( gcloud iam workload-identity-pools describe "idpool-${resource_id}" \
    --location "global" --format "value(name)" )
```

リポジトリに対し、この SA の利用を許可します。

```sh
gitlab_host=gitlab.com
gitlab_repo=<org-id>/<repo-id>
gcloud iam workload-identity-pools providers create-oidc "idp-gitlab-${resource_id}" \
    --workload-identity-pool "idpool-${resource_id}" --location "global" \
    --issuer-uri "https://${gitlab_host}/" --allowed-audiences "https://${gitlab_host}" \
    --attribute-mapping "google.subject=assertion.sub,attribute.project_path=assertion.project_path" \
    --display-name "Workload IdP for GitLab for ${resource_id}"
gcloud iam service-accounts add-iam-policy-binding \
    sa-cicd-${resource_id}@${project_id}.iam.gserviceaccount.com \
    --member "principalSet://iam.googleapis.com/${idp_id}/attribute.project_path/${gitlab_repo}" \
    --role "roles/iam.workloadIdentityUser"
cat << EOF

GOOGLE_CLOUD_PROJECT:
    ${project_id}

GOOGLE_CLOUD_RESOURCE_ID:
    ${resource_id}

GOOGLE_CLOUD_WORKLOAD_IDP:
    $( gcloud iam workload-identity-pools providers describe "idp-gitlab-${resource_id}" \
        --workload-identity-pool "idpool-${resource_id}" --location "global" \
        --format "value(name)" )

EOF
```

プロジェクトの CI/CD Variables に以下の値を設定します。

- GOOGLE_CLOUD_PROJECT: プロジェクト ID
- GOOGLE_CLOUD_RESOURCE_ID: リソース識別 ID
- GOOGLE_CLOUD_WORKLOAD_IDP: Workload Identity の IdP ID
