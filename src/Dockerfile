FROM python:3.12-slim as builder
ENV PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=false
ENV PATH="${POETRY_HOME}/bin:${PATH}"
RUN python -c 'from urllib.request import urlopen; print(urlopen("https://install.python-poetry.org").read().decode())' | python -
RUN poetry config virtualenvs.create false
WORKDIR /app
COPY pyproject.toml /app/
COPY poetry.lock /app/
RUN poetry install --no-interaction --without dev --no-ansi --no-root -vvv

FROM python:3.12-slim as app
ENV PYTHONUNBUFFERED=1 \
    PYTHONIOENCODING="UTF-8"
COPY --from=builder /usr/local/lib/python3.12/site-packages /usr/local/lib/python3.12/site-packages
COPY --from=builder /usr/local/bin/streamlit /usr/local/bin/
WORKDIR /app
COPY . /app/
CMD ["streamlit", "run", "app.py", "--server.port", "8080"]

FROM golang:1.21.3-alpine as gcsfuse
RUN go install github.com/googlecloudplatform/gcsfuse@v1.2.1

FROM app
RUN apt-get update && apt-get install -y --no-install-recommends tini fuse && apt-get clean
COPY --from=gcsfuse /go/bin/gcsfuse /usr/local/bin/
ENV MNT_DIR=/work
RUN chmod +x /app/entrypoint.sh
ENTRYPOINT ["/usr/bin/tini", "--"]
CMD ["/app/entrypoint.sh"]
