#!/usr/bin/env bash
set -eo pipefail

mkdir -p ${MNT_DIR:-/work}
gcsfuse --implicit-dirs --debug_gcs --debug_fuse ${BUCKET} ${MNT_DIR:-/work}

streamlit run app.py --server.port ${PORT:-8080}
